<?php

namespace MediaWiki\Extension\PrzewodnikStyle;

use MediaWiki\Hook\SkinAddFooterLinksHook;
use Skin;
use Html;

class PrzewodnikSkinAndFooterLinksHooks implements SkinAddFooterLinksHook {
    public function onSkinAddFooterLinks( Skin $skin, string $key, array &$footerlinks ){
        if($key === "places"){
            $footerlinks["care"] = Html::element( 
                'a',
                [
                    'href' => 'https://wspolneoparcie.org',
                    'target' => '_blank'
                ],
                $skin->msg( 'project-care' )->text()
            );
        }
    }
}